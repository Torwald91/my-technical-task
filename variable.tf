﻿variable "azure_resource_group_name" {
    description = "Resource Group Name"
    default = "group-lb-wb"
}

variable "vm_name_prefix" { 
	description = "The Virtual Machine Name"
    default = "WinVM_srv"
}

variable "vm_count" {
    description = "Number of VMs to create"
    default = "2"
}

variable "vm_size" { 
	description = "Azure VM Size"
    default = "Standard_DS1_v2"
}

variable "vm_winrm_port" {
    description = "WinRM Public Port"
    default = "5986"
}

variable "azure_region" {
    description = "Azure Region for all resources"
    default = "westeurope"
}

variable "azure_region_fullname" {
    description = "Long name for the Azure Region, ie. North Europe"
    default = "West Europe"
}

variable "admin_username" {
    description = "Username for the Administrator account"
    default = "Azadmin"
}

variable "admin_password" {
    description = "Password for the Administrator account"
    default = "F;Eh13Ctr"
}