﻿terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "=3.0.1"
    }
  }
}


provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "${var.azure_resource_group_name}"
  location = "${var.azure_region_fullname}"

}

resource "azurerm_availability_set" "ava_set" {
  name                         = "${var.vm_name_prefix}-avst"
  location                     = "${var.azure_region_fullname}"
  resource_group_name          = "${azurerm_resource_group.rg.name}"
}

## Raising a virtual network ##

resource "azurerm_virtual_network" "virnet" {
  name                = "${var.azure_resource_group_name}-Vnet"
  address_space       = ["10.21.0.0/16"]
  location            = "${var.azure_region_fullname}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
}

resource "azurerm_subnet" "snet" {
  name                 = "${var.azure_resource_group_name}-Snet"
  resource_group_name  = "${azurerm_resource_group.rg.name}"
  virtual_network_name = azurerm_virtual_network.virnet.name
  address_prefixes       = ["10.21.0.0/24"]
}


## Raising a load balancer ##

# Virtual ip address

resource "azurerm_public_ip" "lb_public_ip" {
  name                         = "${var.vm_name_prefix}-ip"
  location                     = "${var.azure_region_fullname}"
  resource_group_name          = "${azurerm_resource_group.rg.name}"
  allocation_method   = "Dynamic"
}

# Frontend Load Balancer
resource "azurerm_lb" "load_balancer" {
  name                = "${var.vm_name_prefix}-lb"
  location            = "${var.azure_region_fullname}"
  resource_group_name = "${azurerm_resource_group.rg.name}"

  frontend_ip_configuration {
    name                 = "${var.vm_name_prefix}-ipconf"
    public_ip_address_id = azurerm_public_ip.lb_public_ip.id
  }
}

# Backend Address Pool
resource "azurerm_lb_backend_address_pool" "back_pool" {
  name                = "${var.vm_name_prefix}-back_addpool"
  loadbalancer_id     = azurerm_lb.load_balancer.id
}

# LB Rule
resource "azurerm_lb_rule" "lb_http_rule" {
  name                           = "HTTPRule"
  loadbalancer_id                = azurerm_lb.load_balancer.id
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "${var.vm_name_prefix}-ipconf"
  probe_id                       = "${azurerm_lb_probe.loadbalancer_probe.id}"
  depends_on                     = [azurerm_lb_probe.loadbalancer_probe]
}

resource "azurerm_lb_rule" "lb_https_rule" {
  name                           = "HTTPSRule"
  loadbalancer_id                = "${azurerm_lb.load_balancer.id}"
  protocol                       = "Tcp"
  frontend_port                  = 443
  backend_port                   = 443
  frontend_ip_configuration_name = "${var.vm_name_prefix}-ipconfig"
  probe_id                       = "${azurerm_lb_probe.loadbalancer_probe.id}"
  depends_on                     = [azurerm_lb_probe.loadbalancer_probe]
}

#LB Probe - Checking the availability and health of virtual machines
resource "azurerm_lb_probe" "loadbalancer_probe" {
  loadbalancer_id     = "${azurerm_lb.load_balancer.id}"
  name                = "HTTP"
  port                = 80
}

## Create storage acount ##

resource "azurerm_storage_account" "st_account" {
    name = "${var.azure_resource_group_name}-storages"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    location = "${var.azure_region_fullname}"
    account_tier             = "Standard"
    account_replication_type = "LRS"
}

resource "azurerm_storage_container" "container" {
    name = "vhds"
    storage_account_name = "${azurerm_storage_account.st_account.name}"
    container_access_type = "private"
}

## Security for VM ##

resource "azurerm_network_security_group" "vm_sec_group" {
    name = "${var.vm_name_prefix}-secg"
    location = "${var.azure_region_fullname}"
    resource_group_name = "${azurerm_resource_group.rg.name}"
}


resource "azurerm_network_security_rule" "rdp_rule" {
    name = "rdpRule"
    priority = 100
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "3389"
    source_address_prefix = "*"
    destination_address_prefix = "*"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    network_security_group_name = "${azurerm_network_security_group.vm_sec_group.name}"
}

resource "azurerm_network_security_rule" "winrm_rule" {
    name = "winrmRule"
    priority = 110
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "${var.vm_winrm_port}"
    source_address_prefix = "*"
    destination_address_prefix = "*"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    network_security_group_name = "${azurerm_network_security_group.vm_sec_group.name}"
}

resource "azurerm_network_security_rule" "http_rule" {
    name = "HTTP"
    priority = 120
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "80"
    source_address_prefix = "*"
    destination_address_prefix = "*"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    network_security_group_name = "${azurerm_network_security_group.vm_sec_group.name}"
}

resource "azurerm_network_security_rule" "https_rule" {
    name = "HTTPS"
    priority = 130
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "443"
    source_address_prefix = "*"
    destination_address_prefix = "*"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    network_security_group_name = "${azurerm_network_security_group.vm_sec_group.name}"
}


## Raising a Virtual Machines ##

resource "azurerm_lb_nat_rule" "winrm_nat" {
  name = "WinRM-https-vm-${count.index}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  loadbalancer_id = "${azurerm_lb.load_balancer.id}"
  protocol = "Tcp"
  frontend_port = "${count.index + 10000}"
  backend_port = "${var.vm_winrm_port}"
  frontend_ip_configuration_name = "${var.vm_name_prefix}-ipconf"
  count = "${var.vm_count}"
}

resource "azurerm_lb_nat_rule" "rdp_nat" {
  name = "rdp-vm-${count.index}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  loadbalancer_id = "${azurerm_lb.load_balancer.id}"
  protocol = "Tcp"
  frontend_port = "${count.index + 11000}"
  backend_port = "3389"
  frontend_ip_configuration_name = "${var.vm_name_prefix}-ipconf"
  count = "${var.vm_count}"
}


resource "azurerm_network_interface" "vm_nic" {
    name = "${var.vm_name_prefix}-${count.index}-nic"
    location = "${var.azure_region_fullname}"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    count = "${var.vm_count}"

    ip_configuration {
        name = "${var.vm_name_prefix}-${count.index}-ipConf"
        subnet_id = "${azurerm_subnet.snet.id}"
        private_ip_address_allocation = "dynamic"
    }

}

resource "azurerm_virtual_machine" "virtual_machine" {
    name = "${var.vm_name_prefix}-${count.index}"
    location = "${var.azure_region_fullname}"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    network_interface_ids = ["${element(azurerm_network_interface.vm_nic.*.id, count.index)}"]
    vm_size = "${var.vm_size}"
    count = "${var.vm_count}"
    availability_set_id = "${azurerm_availability_set.ava_set.id}"
    delete_os_disk_on_termination = true
    delete_data_disks_on_termination = true

    storage_image_reference {
        publisher = "MicrosoftWindowsServer"
        offer = "WindowsServer"
        sku = "2019-Datacenter"
        version = "latest"
    }

    storage_os_disk {
        name = "${var.vm_name_prefix}-${count.index}-disk"
        vhd_uri = "${azurerm_storage_account.st_account.primary_blob_endpoint}${azurerm_storage_container.container.name}/${var.vm_name_prefix}-${count.index}-disk.vhd"
        caching = "ReadWrite"
        create_option = "FromImage"
    }

    os_profile {
        computer_name = "${var.vm_name_prefix}-${count.index}"
        admin_username = "${var.admin_username}"
        admin_password = "${var.admin_password}"
        custom_data = "${base64encode("Param($RemoteHostName = \"${var.vm_name_prefix}-${count.index}.${var.azure_region}\", $ComputerName = \"${var.vm_name_prefix}-${count.index}\", $WinRmPort = ${var.vm_winrm_port}) ${file("Deploy.PS1")}")}"
     }

    os_profile_windows_config {
        provision_vm_agent = true
        enable_automatic_upgrades = true

        additional_unattend_config {
            pass = "oobeSystem"
            component = "Microsoft-Windows-Shell-Setup"
            setting_name = "AutoLogon"
            content = "<AutoLogon><Password><Value>${var.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_username}</Username></AutoLogon>"
        }
        
        #Authentication in WinRM

        additional_unattend_config {
            pass = "oobeSystem"
            component = "Microsoft-Windows-Shell-Setup"
            setting_name = "FirstLogonCommands"
            content = "${file("FirstLogonCommands.xml")}"
        }
    } 

    provisioner "file" {
        source = "WebserverDsc.PS1"
        destination = "C:\\Scripts\\WebserverDsc.PS1"
        connection {
            type = "winrm"
            https = true
            insecure = true
            user = "${var.admin_username}"
            password = "${var.admin_password}"
            host = "${azurerm_resource_group.rg.name}.${var.azure_region}"
            port = "${count.index + 10000}"
        }
    }

    provisioner "file" {
        content = "${var.vm_name_prefix}-${count.index}"
        destination = "C:\\inetpub\\wwwroot\\default.htm"
        connection {
            type = "winrm"
            https = true
            insecure = true
            user = "${var.admin_username}"
            password = "${var.admin_password}"
            host = "${azurerm_resource_group.rg.name}.${var.azure_region}"
            port = "${count.index + 10000}"
        }
    }

    provisioner "remote-exec" {
      inline = [
        "powershell.exe -sta -ExecutionPolicy Unrestricted -file C:\\Scripts\\WebserverDsc.ps1",
      ]
        connection {
            type = "winrm"
            timeout = "20m"
            https = true
            insecure = true
            user = "${var.admin_username}"
            password = "${var.admin_password}"
            host = "${azurerm_resource_group.resource_group.name}.${var.azure_region}"
            port = "${count.index + 10000}"
        }
    }

}
